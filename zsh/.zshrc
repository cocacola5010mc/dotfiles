
# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/owner/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster"

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#3d4f54,bold"
ZSH_AUTOSUGGEST_STRATEGY=(completion)
ZSH_AUTOSUGGEST_USE_ASYNC=true

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git zsh-autosuggestions)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#GoTo

alias gcode="cd ~/Documents/code"

#TodoLists
alias todo="python3 ~/Documents/Todos/todo.py"
alias td="todo"

#Tmux
alias tmnn="tmux new -s "
alias tmn="tmux new"
alias tman="tmux attach -t "
alias tma="tmux attach "
alias tmkn="tmux kill-session -t "
alias tmk="tmux kill-session "
alias tmls="tmux ls"
alias tml="tmls"

alias tmhelp="tm + <Command abbreviation> + <named marked with n>"

#Java
alias java8="/lib/jvm/java-8-openjdk-amd64/bin/java"
alias javac8="/lib/jvm/java-8-openjdk-amd64/bin/javac"
alias jar8="/lib/jvm/java-8-openjdk-amd64/bin/jar"
alias javadoc8="/lib/jvm/java-8-openjdk-amd64/bin/javadoc"

alias java11="/lib/jvm/java-11-openjdk-amd64/bin/java"
alias javac11="/lib/jvm/java-11-openjdk-amd64/bin/javac"
alias jar11="/lib/jvm/java-11-openjdk-amd64/bin/jar"
alias javadoc11="/lib/jvm/java-11-openjdk-amd64/bin/javadoc"

alias java14="/lib/jvm/java-14-openjdk-amd64/bin/java"
alias javac14="/lib/jvm/java-14-openjdk-amd64/bin/javac"
alias jar14="/lib/jvm/java-14-openjdk-amd64/bin/jar"
alias javadoc14="/lib/jvm/java-14-openjdk-amd64/bin/javadoc"

alias javahelp="echo <Regular Java Command> + <8/11/14>"

#apps
alias cc="java8 -jar /home/owner/Documents/crossclicker/CrossClicker.jar"


#gradle
alias gra="./gradlew"

#forge
alias frc="gra runClient"
alias frs="gra runServer"

alias fhelp="echo command format: G + <Command letters>"

#gotop
alias system="gotop-cjbassi"

#Minecraft

alias mc="vblank_mode=0 minecraft-launcher"
alias minecraft="mc"

#Work Shortcuts

alias api="cd ~/Desktop/vmeet/vmeet-api"
alias front="cd ~/Desktop/vmeet/vmeet-frontend"

#drawing

alias wacom="xsetwacom set 'Wacom Intuos Pro M Pen stylus' MapToOutput DVI-D-0 && xsetwacom set 'Wacom Intuos Pro M Pen stylus' rotate HALF && xsetwacom set 'Wacom Intuos Pro M Finger touch' Touch off && xsetwacom set 'Wacom Intuos Pro M Pen eraser' MapToOutput DVI-D-0 && echo 'Mapped successfully' | lolcat"

# displays

alias displays="xrandr --output DVI-D-0 --left-of HDMI-A-0 && xrandr --output DisplayPort-0 --right-of HDMI-A-0 && xrandr --output HDMI-A-0 --rotate right && hsetroot -cover Pictures/big\ background.png -brightness -0.1 -tint '#002b36' && echo 'Displays Configured!' | lolcat"

# slack
alias slack="slack -s | lolcat"

alias polybridge="'/home/owner/.local/share/Steam/steamapps/common/Poly Bridge/polybridge.x86_64' -noscaling"

alias df="(cd ~/Documents/df_linux/ && sh run.sh)"
