# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Aliases set by me
alias emacs="emacs -nw"
alias cs="cd"
alias c="cd"

#GoTo

alias gcode="cd ~/Documents/code"

#TodoLists
alias todo="python3 ~/Documents/Todos/todo.py"
alias td="todo"

#Tmux
alias tmnn="tmux new -s "
alias tmn="tmux new"
alias tman="tmux attach -t "
alias tma="tmux attach "
alias tmkn="tmux kill-session -t "
alias tmk="tmux kill-session "
alias tmls="tmux ls"
alias tml="tmls"

alias tmhelp="tm + <Command abbreviation> + <named marked with n>"

#Java
alias java8="/lib/jvm/java-8-openjdk-amd64/bin/java"
alias javac8="/lib/jvm/java-8-openjdk-amd64/bin/javac"
alias jar8="/lib/jvm/java-8-openjdk-amd64/bin/jar"
alias javadoc8="/lib/jvm/java-8-openjdk-amd64/bin/javadoc"

alias java11="/lib/jvm/java-11-openjdk-amd64/bin/java"
alias javac11="/lib/jvm/java-11-openjdk-amd64/bin/javac"
alias jar11="/lib/jvm/java-11-openjdk-amd64/bin/jar"
alias javadoc11="/lib/jvm/java-11-openjdk-amd64/bin/javadoc"

alias java14="/lib/jvm/java-14-openjdk-amd64/bin/java"
alias javac14="/lib/jvm/java-14-openjdk-amd64/bin/javac"
alias jar14="/lib/jvm/java-14-openjdk-amd64/bin/jar"
alias javadoc14="/lib/jvm/java-14-openjdk-amd64/bin/javadoc"

alias javahelp="echo <Regular Java Command> + <8/11/14>"

#apps
alias cc="java8 -jar /home/owner/Documents/crossclicker/CrossClicker.jar"


#gradle
alias gra="./gradlew"

#forge
alias frc="gra runClient"
alias frs="gra runServer"

alias fhelp="echo command format: G + <Command letters>"

#gotop
alias system="gotop-cjbassi"

#Minecraft

alias mc="vblank_mode=0 minecraft-launcher"
alias minecraft="mc"

#Work Shortcuts

alias api="cd ~/Desktop/vmeet/vmeet-api"
alias front="cd ~/Desktop/vmeet/vmeet-frontend"

#drawing

alias wacom="xsetwacom set 'Wacom Intuos Pro M Pen stylus' MapToOutput HDMI-A-0 && xsetwacom set 'Wacom Intuos Pro M Pen stylus' rotate HALF && xsetwacom set 'Wacom Intuos Pro M Finger touch' Touch off && xsetwacom set 'Wacom Intuos Pro M Pen eraser' MapToOutput HDMI-A-0 && echo 'Mapped successfully' | lolcat"

# displays

alias displays="xrandr --output DVI-D-0 --left-of HDMI-A-0 && xrandr --output DisplayPort-0 --right-of HDMI-A-0 && hsetroot -cover Pictures/big\ background.png -brightness -0.1 -tint '#002b36' && echo 'Displays Configured!' | lolcat"

# slack
alias slack="slack -s | lolcat"

alias polybridge="'/home/owner/.local/share/Steam/steamapps/common/Poly Bridge/polybridge.x86_64' -noscaling"

# Snap framework

alias snapf="~/.cabal/bin/snap"
alias cabal="/home/owner/.cabal/bin/cabal"
