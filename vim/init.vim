"# Remap hkjl to neio"
"ml guibg=NONE ctermbg=NONE <h> is gone"
"# <e> (end of word) is now <k>"
"# <i> (enter insert mode) is now <j>"
"# <o> (open instert on line below) is now <l>"
noremap n h

noremap k e
noremap e k

noremap j i
noremap i j

noremap l o
noremap o l

noremap N H

noremap E K
noremap K E

noremap J I
noremap I J

noremap O L
noremap L O

let g:ale_completion_enabled = 1

"Plugin stuff"
call plug#begin('~/.local/share/nvim/site/plugged')
Plug 'sbdchd/neoformat'
Plug 'pangloss/vim-javascript'
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'wakatime/vim-wakatime'
Plug 'dense-analysis/ale'
Plug 'neovimhaskell/haskell-vim'
Plug 'leafgarland/typescript-vim'
Plug 'morhetz/gruvbox'
call plug#end()

"Haskell"
syntax on
filetype plugin indent on

"ALE"

call ale#completion#Disable()

set omnifunc=ale#completion#OmniFunc

let g:ale_linters = {
\   'haskell': ['hdevtools'],
\ }

"Fzf"
nnoremap <C-p> :GFiles<CR>

"Themes"
set background=dark
colorscheme gruvbox
hi Normal guibg=NONE ctermbg=NONE
set number

"Prettier"
autocmd BufWritePre,InsertLeave *.js Neoformat
autocmd BufWritePre,InsertLeave *.html Neoformat
autocmd BufWritePre,InsertLeave *.css Neoformat

"Moving arroud when in insert"
imap <C-e> <Up>
imap <C-n> <Left>
imap <C-o> <Right>
imap <C-i> <Down>

"Make vim use tabs"
set autoindent
set expandtab
set tabstop=2
set shiftwidth=2

"Window Tabs"
map <leader>tn :tabnew<cr>
map <leader>t<leader> :tabnext
map <leader>tm :tabmove
map <leader>tc :tabclose<cr>
map <leader>to :tabonly<cr>

"True Color"
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
